package com.example.omsfour;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OmsFourApplication {

	public static void main(String[] args) {
		SpringApplication.run(OmsFourApplication.class, args);
	}

}
